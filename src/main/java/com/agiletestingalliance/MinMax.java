package com.agiletestingalliance;

public class MinMax {

    public int getMax(int num1, int num2) {
        if (num2 > num1) {
            return num2;
	}
        else {
            return num1;
	}
    }

    public int getMin(int num1, int num2) {
        if (num2 < num1) {
            return num2;
	}
        else {
            return num1;
	}
    }

}
