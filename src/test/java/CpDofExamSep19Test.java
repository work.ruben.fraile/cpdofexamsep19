package com.agiletestingalliance;
import static org.junit.Assert.*;
import java.io.*;
import org.junit.Test;

public class CpDofExamSep19Test {

    @Test
    public void testGetMax() throws Exception {
        int k= new MinMax().getMax(8,5);
        assertEquals("Problem with getMax function:", 8, k);
    }

    @Test
    public void testGetMin() throws Exception {
        int k= new MinMax().getMin(8,5);
        assertEquals("Problem with getMin function:", 5, k);
    }
}
